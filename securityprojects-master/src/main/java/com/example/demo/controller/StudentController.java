package com.example.demo.controller;

import com.example.demo.models.Student;
import com.example.demo.services.EmailService;
import com.example.demo.services.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/students")
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;
    private final EmailService emailService;


    // find all
    @GetMapping
    public String showAllStudents(Model model) {

        model.addAttribute("allStudents", studentService.findAll());

        return "students";
    }

    // save
    @GetMapping("/save/page")
    public String showSavePage(Model model) {

        model.addAttribute("newStudent", new Student());

        return "savepage";
    }

    @PostMapping("/save")
    public String saveStudent(Student student) {

        studentService.save(student);
        emailService.sendEmail(student.getEmail(), student.getName(), "Вы успешно регистраций");
        return "redirect:/api/students";
    }


    // delete
    @GetMapping("/delete/{studentId}")
    public String deleteStudent(@PathVariable Long studentId) {

        studentService.deleteById(studentId);

        return "redirect:/api/students";
    }


    @GetMapping("/update/{id}")
    public String editStudent(Model model, @PathVariable Long id) {
        Student student = studentService.findById(id);
        model.addAttribute("student", student);
        return "update";
    }

    @PostMapping("/update/{id}")
    public String updateStudent(Student student,
                                @PathVariable Long id) {
        studentService.update(student, id);
        return "redirect:/api/students";
    }

}
