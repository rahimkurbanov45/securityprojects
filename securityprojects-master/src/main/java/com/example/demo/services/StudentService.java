package com.example.demo.services;

import com.example.demo.models.Student;
import com.example.demo.repositories.StudentRepo;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class StudentService {
    private final StudentRepo studentRepo;
    private final PasswordEncoder passwordEncoder;


    public List<Student> findAll() {
        return studentRepo.findAll();
    }

    @Transactional
    public void save(Student student) {


        String email = student.getEmail();

        boolean exists = studentRepo.existsByEmail(email);

        if (exists) {
            throw new IllegalStateException(
                    String.format("client with email = %s has already exists", email)
            );
        }
        String encodedPassword = passwordEncoder.encode(student.getPassword());
        student.setPassword(encodedPassword);


        Student save = studentRepo.save(student);

    }


    public void deleteById(Long studentId) {
        studentRepo.deleteById(studentId);
    }


    public Student findById(Long id) {
        return studentRepo.findById(id).orElseThrow(() -> new IllegalStateException("id not found"));
    }

    @Transactional
    public void update(Student student, Long id) {
        Student student1 = studentRepo.findById(id).orElseThrow(() -> new IllegalStateException("this id not found"));
        student1.setName(student.getName());
        student1.setPassword(passwordEncoder.encode(student.getPassword()));
        student.setEmail(student.getEmail());
    }
}

